<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$con = mysqli_connect("localhost","root","","social");

if(mysqli_connect_errno()){
	echo "Failed to connect: " . mysqli_connect_errno();
}

//$query = mysqli_query($con, "INSERT INTO test VALUES(NULL, 'Optimus Prime')"); 

//declaring variables to prevent errors
$fname ="";
$lname ="";
$em = "";
$em2 = "";
$password = "";
$password2 = "";
$date = "";
$error_array = array();


if(isset($_POST['register_button'])){
	//Reg form values

	//first name
	$fname = strip_tags($_POST['reg_fname']);
	$fname = str_replace(' ', '', $fname);
	$fname = ucfirst(strtolower($fname));
	$_SESSION['reg_fname']= $fname;

	//last name
	$lname = strip_tags($_POST['reg_lname']);
	$lname = str_replace(' ', '', $lname);
	$lname = ucfirst(strtolower($lname));
	$_SESSION['reg_lname']= $lname;


	//em
	$em = strip_tags($_POST['reg_email']);
	$em = str_replace(' ', '', $em);
	$em = ucfirst(strtolower($em));
	$_SESSION['reg_email']= $em;
	//em2
	$em2 = strip_tags($_POST['reg_email2']);
	$em2 = str_replace(' ', '', $em2);
	$em2 = ucfirst(strtolower($em2));
	$_SESSION['reg_email2']= $em2;

	$password = strip_tags($_POST['reg_password']);
	$password2 = strip_tags($_POST['reg_password2']);	

	$date = date("Y-m-d");

	if($em == $em2){
		if(filter_var($em, FILTER_VALIDATE_EMAIL)){
			$em = filter_var($em, FILTER_VALIDATE_EMAIL);

			//check if email already exist
			$e_check = mysqli_query($con, "SELECT email FROM users WHERE email='$em'");

			//count the number of rows returned:
			$num_rows = mysqli_num_rows($e_check);

			if($num_rows>0){
				array_push($error_array, "Email already is use<br>");
			}
		}
		else{
			array_push($error_array, "Invalid email format<br>");
		}
	}
	else{
		array_push($error_array,  "Emails don't match<br>");
	}


	if(strlen($fname)>25 || strlen($fname)<2){
		array_push($error_array, "You first name must be between 2 and 25 charackters<br>");
	}

	if(strlen($lname)>25 || strlen($lname)<2){
		array_push($error_array, "You last name must be between 2 and 25 charackters<br>");
	}

	if($password != $password2){
		array_push($error_array, "Password don't match<br>");
	}
	else{
		if(preg_match('/[^A-Za-z0-9]/', $password)){
			array_push($error_array, "your pass can only containt english characket or number <br>");
		}
	}

	if(strlen($password)>30 || strlen($password)<5){
		array_push($error_array, "You pass must be between 5 and 25 charackters<br>");
	}


	if(empty($error_array)){
		$password = md5($password); // Encrypt pass before sending to database;

		//gen username by first name and last name for example:
		$username = strtolower($fname . "_". $lname);
		$check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username='$username'");

		$i = 0;
		//if username exists add number to username
		while(mysqli_num_rows($check_username_query) != 0){
			$i++;
			$username .= "_" . $i;
			$check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username='$username'");
		}
		//Profile picture assignment:
		$rand = rand(1,2); //rand number between 1 and 2 ;

		if($rand == 1) $profile_pic = "assets/images/profile_pic/defaults/head_deep_blue.png"; //end her
		else if($rand ==2) $profile_pic = "assets/images/profile_pic/defaults/head_emerald.png"; //end here


		$query = mysqli_query($con, "INSERT INTO users VALUES('', '$fname', '$lname', '$username', '$em', '$password', '$profile_pic', '0', '0','no', ',')");

		array_push($error_array, "<span style='color:green'> You're all set! Goahead and login! </span><br>!");
	}
}

?>


<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<title>Register </title>
	<script>document.write('<script src="http://' + (location.host || '${1:localhost}').split(':')[0] + ':${2:35729}/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>
	<form action="register.php" method="POST">
		<input type="text" name="reg_fname" placeholder="Frist name" value=
		"<?php if(isset($_SESSION['reg_fname']))echo $_SESSION['reg_fname']; ?>" required>
		<br>
		<?php if(in_array("You first name must be between 2 and 25 charackters<br>", $error_array)) echo "You first name must be between 2 and 25 charackters<br>";	?>

		<input type="text" name="reg_lname" placeholder="Last name" value=
		"<?php if(isset($_SESSION['reg_lname'])) echo $_SESSION['reg_lname']; ?>" required>
		<br>
		<?php if(in_array("You last name must be between 2 and 25 charackters<br>", $error_array)) echo "You last name must be between 2 and 25 charackters<br>";	?>
		
		<input type="email" name="reg_email" placeholder="Emial" value=
		"<?php if(isset($_SESSION['reg_email'])) echo $_SESSION['reg_email']; ?>" required>
		<br>
		<?php if(in_array("Email already is use<br>", $error_array)) echo "Email already is use<br>";	
		else if(in_array("Invalid email format<br>", $error_array)) echo "Invalid email format<br>";	
		else if(in_array("Emails don't match<br>", $error_array)) echo "Emails don't match<br>";	?>
		<input type="email" name="reg_email2" placeholder="Email2" value=
		"<?php if(isset($_SESSION['reg_email2']))echo $_SESSION['reg_email2'];?>" required>
		<br>
		<input type="password" name="reg_password" placeholder="Password" required>
		<br>
		<input type="password" name="reg_password2" placeholder="Password2" required>
		<br>
		<?php 
		if(in_array("Password don't match<br>", $error_array)) echo "Password don't match<br>";
		else if(in_array("your pass can only containt english characket or number <br>", $error_array)) echo "your pass can only containt english characket or number <br>";
		else if(in_array("You pass must be between 5 and 25 charackters<br>", $error_array)) echo "You pass must be between 5 and 25 charackters<br>";	?>
		<input type="submit" name="register_button" value="Register">
		<?php if(in_array("<span style='color:green'> You're all set! Goahead and login! </span><br>!", $error_array)) echo "<span style='color:green'> You're all set! Goahead and login! </span><br>!";
		?>
	</form>
</body>
</html>